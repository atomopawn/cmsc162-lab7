CFLAGS=-g
OBJDIR=obj

Sort: main.cpp $(OBJDIR)/bucket_sorter.o $(OBJDIR)/sorter.o
	g++ ${CFLAGS} main.cpp $(OBJDIR)/bucket_sorter.o $(OBJDIR)/sorter.o -o Sort

tests: test0

$(OBJDIR)/%.o: %.cpp %.h
	g++ ${CFLAGS} -c $< -o $@

.PHONY: clean test1 test2 test3 test4 test5

clean: 
	rm -rf obj/*.o Sort test*.data

test0: Sort test0.input test0.output
	@echo Test Case 0
	@./Sort test0.input 
	@diff test0.data test0.output && echo "Test passed"
	@echo 

test1: Sort test1.input test1.output
	@echo Test Case 1
	@./Sort test1.input 
	@diff test1.data test1.output && echo "Test passed"
	@echo 

test2: Sort test2.input test2.output
	@echo Test Case 2
	@./Sort test2.input 
	@diff test2.data test2.output && echo "Test passed"
	@echo 

test3: Sort test3.input test3.output
	@echo Test Case 3
	@./Sort test3.input 
	@diff test3.data test3.output && echo "Test passed"
	@echo 

test4: Sort test4.input test4.output
	@echo Test Case 4
	@./Sort test4.input 
	@diff test4.data test4.output && echo "Test passed"
	@echo 

test5: Sort test5.input test5.output
	@echo Test Case 5
	@./Sort test5.input 
	@diff test5.data test5.output && echo "Test passed"
	@echo 
