#include <iostream>

using namespace std;

class Sorter
{
	protected:
		int* m_data;
		int m_size;
	public:
		Sorter();
		Sorter(const string filename);
		Sorter(const Sorter& other);
		Sorter(const Sorter&& other);
		Sorter& operator=(const Sorter& other);
		Sorter& operator=(const Sorter&& other);
		~Sorter();
		void read(const string filename);
		void write(const string filename);

		virtual void sort() = 0;
};
