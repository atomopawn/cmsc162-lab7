#include <cassert>
#include "bucket_sorter.h"

BucketSorter::BucketSorter() : Sorter(), m_buckets{new int[NUM_BUCKETS]}
{ 
	for (int i=0; i < NUM_BUCKETS; ++i) {
		m_buckets[i] = 0;
	}
}

BucketSorter::BucketSorter(const string& filename) : Sorter(filename), m_buckets{new int[NUM_BUCKETS]}
{ 
	for (int i=0; i < NUM_BUCKETS; ++i) {
		m_buckets[i] = 0;
	}
}

BucketSorter::~BucketSorter() 
{
	delete[] m_buckets;
}

void BucketSorter::sort()
{
	for (int i=0; i < m_size; ++i) {
		assert(m_data[i] >= 0);
		assert(m_data[i] < NUM_BUCKETS);
		++m_buckets[m_data[i]];	
	}
	int k = 0;
	for (int i=0; i < NUM_BUCKETS; ++i) {
		for (int j=0; j < m_buckets[i]; ++j) {
			m_data[k] = i;
			++k;
		}
	}
	assert(k == m_size);
}
