#include "sorter.h"

const int NUM_BUCKETS = 1048576;

class BucketSorter : public Sorter
{
	private:
		int* m_buckets;
	public:
		BucketSorter();
		BucketSorter(const string& filename);
		~BucketSorter();
		void sort();
};
