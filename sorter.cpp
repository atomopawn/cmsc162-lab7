#include "sorter.h"
#include <fstream>

Sorter::Sorter() : m_size{0}, m_data{nullptr}
{ }

Sorter::Sorter(const string filename) : m_size{0}, m_data{nullptr}
{
	read(filename);
}

Sorter::Sorter(const Sorter& other) : m_size{other.m_size}, m_data{new int[m_size]}
{
	for (int i=0; i < m_size; ++i) {
		m_data[i] = other.m_data[i];
	}
}

Sorter::Sorter(const Sorter&& other) : m_size{other.m_size}, m_data{other.m_data}
{ }

Sorter::~Sorter()
{
	if (m_data)
		delete[] m_data;
}

Sorter& Sorter::operator=(const Sorter& other)
{
	m_size = other.m_size;

	if (m_data)
		delete[] m_data;
	m_data = new int[m_size];

	for (int i=0; i < m_size; ++i) {
		m_data[i] = other.m_data[i];
	}
	return *this;
}

Sorter& Sorter::operator=(const Sorter&& other) {
	m_size = other.m_size;

	if (m_data)
		delete[] m_data;

	m_data = other.m_data;
	return *this;
}

void Sorter::read(const string filename)
{
	ifstream fin(filename);
	fin >> m_size;
	if (m_data) {
		delete[] m_data;
	}
	m_data = new int[m_size];
	for (int i=0; i < m_size; ++i) {
		fin >> m_data[i];
	}
}

void Sorter::write(const string filename)
{
	ofstream fout(filename);
	for (int i=0; i < m_size-1; ++i)
	{
		fout << m_data[i] << " ";
	}
	if (m_size > 0) {
		fout << m_data[m_size-1] << endl;
	}
}
