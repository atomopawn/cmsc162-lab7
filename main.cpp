#include "bucket_sorter.h"
#include <cassert>
#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{
	if (argc < 2) {
		cout << "Syntax: ./Sort <testX.input>" << endl;
		return 1;
	}

	//Read input filename from command line
	string input_file = argv[1];

	//Check that it ends with .input
	size_t pos = input_file.find(".input");
	if (pos != input_file.size() - 6) {
		cout << "Syntax: ./Sort <testX.input>" << endl;
		return 1;
	}
	
	//If it does, replace .input with .data in the output name
	string output_file = input_file.substr(0, input_file.size()-6) + ".data";

	//Create a Sorter and read all the data from the input file
	//Sort the data
	//Then write the data to the output file
	BucketSorter bs(input_file);
	bs.sort();
	bs.write(output_file);
}
